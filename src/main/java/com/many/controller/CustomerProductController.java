package com.many.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.many.Entity.Customer;
import com.many.Entity.Product;
import com.many.Repository.CustomerRepository;
import com.many.Repository.ProductRepository;

@RestController
@RequestMapping("/customer/course")
public class CustomerProductController {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@PostMapping
	public Customer saveCustomerWithFruits(@RequestBody Customer customer ) {
		Customer customer2 =customerRepository.save(customer);
		return customer2;
	}
	
	@GetMapping
	public List<Customer> findAllCustomer(){
		return customerRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Customer findById(@PathVariable Long id ) {
		return customerRepository.findById(id).orElse(null);
	}
	
	@GetMapping("/find/{name}")
	public List<Customer> findByName(@PathVariable  String name){
		List<Customer> customers = customerRepository.findByName(name);
		return customers;
	}
	
	@GetMapping("/search/{price}")
	public List<Product> findbyPrice(@PathVariable  double price ){
		List<Product> list =productRepository.findByPriceLessThan(price);
		return list;
	}
}
