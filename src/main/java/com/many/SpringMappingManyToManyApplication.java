package com.many;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMappingManyToManyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMappingManyToManyApplication.class, args);
	}

}
