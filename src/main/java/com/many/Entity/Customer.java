package com.many.Entity;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Customer_Table")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
// While Working WIth Entity mapping instead of data mention in particular

public class Customer {
			
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private int age;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "Customer_product", joinColumns = {
			@JoinColumn(name = "customer_id", referencedColumnName = "id") }, 
	inverseJoinColumns = {
					@JoinColumn(name = "product_id", referencedColumnName = "id") })
	@JsonManagedReference
	private Set<Product> products;

}
