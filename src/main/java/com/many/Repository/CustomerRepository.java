package com.many.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.many.Entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
		
	List<Customer> findByName( String name );

}
